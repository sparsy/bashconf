# Loaders #

# Load app configurations and scripts

# export default EDITOR
export EDITOR='nano'

# VIRTUALENV
if which "virtualenvwrapper.sh" &> /dev/null; then
    export WORKON_HOME=~/.virtualenvs
    if [ ! -f ~/.virtualenvs ]; then
        mkdir -p ~/.virtualenvs
    fi
    source `which virtualenvwrapper.sh`
fi

# CDABLE ALIASES
shopt -s cdable_vars

# autoenv
if which "activate.sh" &> /dev/null; then
    source `which activate.sh`
fi

# use scripts from ~/.local/bin
export PATH=~/.local/bin:$PATH

# pip bash completion
if pip_exec="$(type -p "pip")"; then
    source ~/.bashconf/scripts/pip_complete.sh
fi

# django bash completion
if [ -f ~/.bashconf/scripts/django_completion.sh ]; then
    source ~/.bashconf/scripts/django_completion.sh
fi

# pipsi bash completion
if pipsi_exec="$(type -p "pipsi")"; then
    eval "$(_PIPSI_COMPLETE=source /usr/local/bin/pipsi)"
fi

# projects manager
if [ -f ~/.bashconf/scripts/projects_manager.sh ]; then
    source ~/.bashconf/scripts/projects_manager.sh
fi

# Tmux Connect Script
if tmux_exec="$(type -p "tmux")"; then
    source ~/.bashconf/scripts/tmux_connect.sh
fi
