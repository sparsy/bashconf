# Aliases #

# Common aliases and function for helping the workflow

# Jump back to parent folders
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'

# 'ls' aliases for systems missing them
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias tmp='cd /tmp'

alias find-process='ps -aux | grep'

alias py='python'
alias ipy='ipython'

search () { ll | grep $1; }
deep_search () { tree -af -i | grep $1; }

# Search a filename up until root in every parent directory
find_file_up () {
    path=$(pwd)
    while [[ "$path" != "" && ! -e "$path/$1" ]]; do
        path=${path%/*}
    done
    echo "$path/$1"
}

alias edit="${EDITOR}"
alias rebash='source ~/.bashrc'
alias edbash='edit ~/.bashrc && rebash'

# Check own Local and Public IP
alias my-ip='hostname -I'
alias public-ip='curl icanhazip.com'

# function for easy local configuration using a printed help
# Made with autoenv/direnv in mind
set_local_alias() {
    if [[ $# -eq 0 ]]; then
        echo "Create an alias with a dynamic configuration and display so you"
        echo "can see how it can be used."
        echo
        echo "Usage:"
        echo "    set_local_alias 'ALIAS' 'COMMAND' '[OPTIONAL DESCRIPTION]'"
        echo
        echo "This will output the command defined so it can be used in any autorun file"
        echo "defined in project folder."
    else
        alias $1="${2}"
        echo "< ${1} > ${3:-$2}"
    fi
}

# function to allow conditional execution of command for install of
# check progesses
ask_confirm() {
    if [[ $# -eq 0 ]]; then
        echo "Ask confirmation and run if responded with 'Yes'"
        echo
        echo "Usage:"
        echo '    ask_confirm "Question" "command string to run on Yes" "no command [OPTIONAL]"'
    else
        while true; do
            read -p "$1 [Y/n] " yn
            case $yn in
                [Yy]* )
                    eval ${2-"echo Nothing to do"}
                    break;;
                [Nn]* )
                    eval ${3-"echo Nothing to do"}
                    break;;
                * )
                    echo "Please answer yes or no.";;
            esac
        done
    fi
}
