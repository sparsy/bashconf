# Paths (in alphabetical order) #

# NGINX
nga=/etc/nginx/sites-available
nge=/etc/nginx/sites-enabled
ngl=/var/log/nginx

# POSTGRES
pgl=/var/log/postgresql

# PJ
pj=~/projects

# SENTRY
sentryconf=/etc/sentry

# UWSGI
uwa=/etc/uwsgi/apps-available
uwe=/etc/uwsgi/apps-enabled
uwl=/var/log/uwsgi

# VIRTUALENV
virtualenvs=~/.virtualenvs

# SERVICES
srv=/etc/systemd/system
