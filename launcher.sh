# Launcher #

# Check and load the configuration files

alias edlauncher='$EDITOR ~/.bashconf/launcher.sh && rebash'

alias edloaders='$EDITOR ~/.bashconf/loaders.sh && rebash'
. ~/.bashconf/loaders.sh

alias edpaths='$EDITOR ~/.bashconf/paths.sh && rebash'
. ~/.bashconf/paths.sh

alias edaliases='$EDITOR ~/.bashconf/aliases.sh && rebash'
. ~/.bashconf/aliases.sh

if [ -f ~/.bashconf/local.sh ]; then
    alias edlocal='$EDITOR ~/.bashconf/local.sh && rebash'
    . ~/.bashconf/local.sh

elif [ -f ~/.bashconf_local.sh ]; then
    alias edlocal='$EDITOR ~/.bashconf_local.sh && rebash'
    . ~/.bashconf_local.sh
fi
