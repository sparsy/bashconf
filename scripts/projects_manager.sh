# Projects manager #

# This function let you cd to ~/projects and all
# the subdirectories from anywhere.

# Taken from: https://superuser.com/a/650781

PJ_ALIAS_MOUNT_COMMAND='--alias-mount'
PJ_ALIAS_MOUNT_SHORT_COMMAND='-am'

PJ_TAGS_COMMAND='--tag'
PJ_TAGS_SHORT_COMMAND='-t'

PJ_ALIAS_COMMAND='--alias'
PJ_ALIAS_SHORT_COMMAND='-a'

PJ_TAGS_PREFIX_DEFAULT="+"
PJ_ALIAS_PREFIX_DEFAULT="@"

# TODO: Add help text command
function pj {
    PJ_TAGS_PREFIX="${PJ_TAGS_PREFIX_OVERRIDE:-$PJ_TAGS_PREFIX_DEFAULT}"
    PJ_ALIAS_PREFIX="${PJ_ALIAS_PREFIX_OVERRIDE:-$PJ_ALIAS_PREFIX_DEFAULT}"
    pj_path="${PJ_PATH:-$HOME/projects/}"
    tags_path="${pj_path}.tags/"
    aliases_path="${pj_path}.aliases/"

    if [[ $1 == $PJ_TAGS_COMMAND || $1 == $PJ_TAGS_SHORT_COMMAND ]]; then
        tag_pj_alias_name=${3:-$(basename $PWD)}
        tag_pj_path=$(realpath ${4:-$PWD})
        tag_pj_name=${2:-quick}
        tag_pj_dir=$(basename $tag_pj_path)
        echo "Tagging '${tag_pj_dir}' with '${PJ_TAGS_PREFIX}${tag_pj_name}' as '${tag_pj_alias_name}'"
        tag_pj_dest=$tags_path$tag_pj_name/${tag_pj_alias_name}
        mkdir -p $tags_path$tag_pj_name
        ln -s $tag_pj_path $tag_pj_dest

    elif [[ $1 == $PJ_ALIAS_COMMAND || $1 == $PJ_ALIAS_SHORT_COMMAND ]]; then
        alias_pj_path=$(realpath ${3:-$PWD})
        alias_pj_name=${2:-$(basename $alias_pj_path)}
        echo "Aliasing '${alias_pj_path}' as '${PJ_ALIAS_PREFIX}${alias_pj_name}'"
        alias_pj_dest=$aliases_path$alias_pj_name
        ln -s $alias_pj_path $alias_pj_dest

    elif [[ $1 == $PJ_ALIAS_MOUNT_COMMAND || $1 == $PJ_ALIAS_MOUNT_SHORT_COMMAND ]]; then
        ls -l $HOME/projects/.aliases/ | tail -n +2 | awk '{ print "export", $9, $10, $11 }' | tr '\n' ';' | sed 's/\s->\s/\=/g'

    else
        if [ "${1::1}" == "${PJ_TAGS_PREFIX}" ]; then
            cd -P "${tags_path}${1:1}"
        elif [ "${1::1}" == "${PJ_ALIAS_PREFIX}" ]; then
            cd -P "${aliases_path}${1:1}"
        else
            cd -P $pj_path/$1
        fi
    fi
}

function _pj_get_projects() {
    local cmd curb cur opts
    pj_path="${PJ_PATH:-$HOME/projects/}"

    # get list of directories (use commened out line for dirs and files)
    # append '/' to directories
    # cmd="find $pj_path$curb -maxdepth 1 -type d -printf %p/\n , -type f -print "
    cmd="find -L $pj_path$1 -maxdepth 1 -type d -printf %p/\n "
    # remove base dir from list and remove extra trailing /s
    path_opts=$($cmd 2>/dev/null | sed s:$pj_path:: | sed s://*$:/:)
    echo $path_opts
}

function _pj_get_tags() {
    local cmd curb cur opts
    PJ_TAGS_PREFIX="${PJ_TAGS_PREFIX_OVERRIDE:-$PJ_TAGS_PREFIX_DEFAULT}"
    pj_path="${PJ_PATH:-$HOME/projects/}"
    tags_path="${pj_path}.tags/"

    tags_cmd="find -L $tags_path -mindepth 2 -maxdepth 2 -type d -printf %p\n "
    tags_opts=$($tags_cmd | sed s:$tags_path:: | sed s:/$:/: | sed s:^:"$PJ_TAGS_PREFIX":)
    echo $tags_opts
}

function _pj_get_aliases() {
    local cmd curb cur opts
    PJ_ALIAS_PREFIX="${PJ_ALIAS_PREFIX_OVERRIDE:-$PJ_ALIAS_PREFIX_DEFAULT}"
    pj_path="${PJ_PATH:-$HOME/projects/}"
    aliases_path="${pj_path}.aliases/"

    alias_cmd="find -L $aliases_path -mindepth 1 -maxdepth 1 -type d -printf %p\n "
    aliases_opts=$($alias_cmd | sed s:$aliases_path:: | sed s:/:: | sed s:^:"$PJ_ALIAS_PREFIX":)
    echo $aliases_opts
}

function _pjcomplete()
{
    local cmd curb cur opts

    #PJ_TAGS_PREFIX="${PJ_TAGS_PREFIX_OVERRIDE:-$PJ_TAGS_PREFIX_DEFAULT}"
    #PJ_ALIAS_PREFIX="${PJ_ALIAS_PREFIX_OVERRIDE:-$PJ_ALIAS_PREFIX_DEFAULT}"
    # base dir
    #pj_path="${PJ_PATH:-$HOME/projects/}"
    #tags_path="${pj_path}.tags/"
    #aliases_path="${pj_path}.aliases/"

    # last arg so far
    cur="${COMP_WORDS[COMP_CWORD]}"

    # dirname-esque, but makes "xyz/" => "xyz/" not "."
    curb=$(echo $cur | sed 's,[^/]*$,,')

    # get list of directories (use commened out line for dirs and files)
    # append '/' to directories
    # cmd="find $pj_path$curb -maxdepth 1 -type d -printf %p/\n , -type f -print "
    #cmd="find -L $pj_path$curb -maxdepth 1 -type d -printf %p/\n "
    #tags_cmd="find -L $tags_path -mindepth 2 -maxdepth 2 -type d -printf %p\n "
    #alias_cmd="find -L $aliases_path -mindepth 1 -maxdepth 1 -type d -printf %p\n "

    # remove base dir from list and remove extra trailing /s
    #path_opts=$($cmd 2>/dev/null | sed s:$pj_path:: | sed s://*$:/:)
    #tags_prefix=$PJ_TAGS_PREFIX
    #tags_opts=$($tags_cmd | sed s:$tags_path:: | sed s:/$:/: | sed s:^:"$tags_prefix":)
    #alias_prefix=$PJ_ALIAS_PREFIX
    #aliases_opts=$($alias_cmd | sed s:$aliases_path:: | sed s:/:: | sed s:^:"$alias_prefix":)

    # Extract options for all the paths and links
    opts="$(_pj_get_projects $curb) $(_pj_get_tags) $(_pj_get_aliases)"

    # generate list of completions
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    return 0;
}

_PJ_TAGS_COMMANDS="${PJ_TAGS_COMMAND} ${PJ_TAGS_SHORT_COMMAND}"
_PJ_ALIASES_COMMANDS="${PJ_ALIAS_COMMAND} ${PJ_ALIAS_SHORT_COMMAND}"
_PJ_ALIASES_MOUNT_COMMANDS="${PJ_ALIAS_MOUNT_COMMAND} ${PJ_ALIAS_SHORT_COMMAND}"
complete -o nospace -F _pjcomplete -W "${_PJ_TAGS_COMMANDS} ${_PJ_ALIASES_COMMANDS} ${_PJ_ALIASES_MOUNT_COMMANDS}" pj
