# Tmux connect #

function tmux-connect {
    if [ -z $1 ] || [ $1 = '-h' ] || [ $1 = '--help' ]; then
        echo "Usage: tmux-connect HOST GUEST";
        echo "";
        echo "  Easily connect to a tmux session for pair programming";
        echo "";
        echo "How to use:";
        echo "  -h, --help    Show this help text";
        echo "  HOST          Create a session named HOST";
        echo "  HOST GUEST    Create a session named GUEST and connect it to HOST";
    elif [ -z $2 ]; then # if is zero or empty
        tmux new -s $1;
    else
        tmux new -s $2 -t $1;
    fi;
}