#!/usr/bin/env bash

git_command=$(command -v git)

if ! [[ -f $git_command ]]; then
    echo "Git is required to install Bashconf"
    exit 0
fi

TARGET_CONFIG=~/.bashrc
TARGET_REPO_PATH=~/.bashconf

BEGIN_LINE="# BEGIN BASHCONF ACTIVATION BLOCK"
END_LINE="# END BASHCONF ACTIVATION BLOCK"

SCRIPT="
$BEGIN_LINE
if [ -f $TARGET_REPO_PATH/launcher.sh ]; then
    . $TARGET_REPO_PATH/launcher.sh
fi
$END_LINE"

if [ -d $TARGET_REPO_PATH ]; then
    echo "Bashconf already cloned"
else
    echo "Cloning bashconf"
    git clone https://gitlab.com/sparsy/bashconf.git $TARGET_REPO_PATH
fi

if grep -qxF "$BEGIN_LINE" $TARGET_CONFIG; then
    echo "Bashconf already installed"
else
    echo "Bashconf activation script added to $TARGET_CONFIG"
    echo "$SCRIPT" >> $TARGET_CONFIG

    cp $TARGET_REPO_PATH/local.sh.template $TARGET_REPO_PATH/local.sh
    echo "Edit ${TARGET_REPO_PATH}/local.sh for local configurations"

    source $TARGET_REPO_PATH/launcher.sh
    echo "Bashconf sourced and ready to use"
fi


