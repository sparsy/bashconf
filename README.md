bashconf
========

clean structure for variables, configurations, aliases and functions

Install
-------
To install easily just run the folowing command:

    curl https://gitlab.com/sparsy/bashconf/-/raw/master/install.sh | bash

Manual Install
-------
First, clone the project in the your `home` folder:

    git clone https://gitlab.com/sparsy/bashconf.git ~/.bashconf

Then edit the `~/.bashrc` by adding these lines:

```bash
if [ -f ~/.bashconf/launcher.sh ]; then
    . ~/.bashconf/launcher.sh
fi
```


Different location
------------------
If you want to keep **bashconf** in a different location just clone it where you want:

    git clone https://gitlab.com/sparsy/bashconf.git absolute/path/to/bashconf

Then create a symbolic link in the `home` folder:

    ln -s absolute/path/to/bashconf ~/.bashconf


local.sh
--------
**bashconf** have a rigid structure that should not be modified directly in production.
So, if you want to redefine or create custom aliases and variables clone the `local.sh.template` as `local.sh`:

    cp ~/.bashconf/local.sh.template ~/.bashconf/local.sh

Then put there all your own declarations.
